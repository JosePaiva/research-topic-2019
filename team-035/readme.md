# EDOM - Project Research Topic

In the next lines it is described all the research made about one of the topics chosen from the given list with over more than 30. This research was conducted to determine more about Domain Engineering and it's approaches.
The topic in the title can be consulted on gitHub (https://github.com/fuinorg/org.fuin.dsl.ddd).

## Brief Introduction

The DDD DSL uses files with .ddd extension to generate packages and classes using a DDD approach. This dsl enables the definition of entities, value objects, aggregates and other concepts of domain driven design. The generation is as described bellow:

![ddddsl](imgs/ddd-dsl-big-pcture.png)

The configurations of the code generation are on srcgen4j-config.xml file. In this file we can see the mapping of the artifacts (Entity, Exception, etc) to their factories.

As an Xtext based DSL it can be parsed, linked, typechecked and also compiled. This allows companies to improve their development productivity by establishing a communication between developers and domain experts. This particular tool is very useful to describe how the system works or will work, and despite being quite complicated, it
has a high payoff.

## Scenario

The DDD DSL allows the development of a domain model application, in a language that, both domain experts and developers, can understand. Since they both can understand it, domain experts analyse the business and put their ideas in the DSL, without the need of knowing the code for the final application. This process also allows the use of validations of attributes of a domain object, being very similar to the OCL language in ecore models.

This DSL can be used to make transformations between domain objects and code base. That is, it allows the generation of code that give the developers a code base, on which they can build an application.

Based on all this previous knowledge an example was made with this particular DSL. It was also made a very simple domain model to simulate a "real-time" usage of the DSL and understand it's capabilities.

#### Contacts Domain Model

This domain model represents an application to manage user contacts.

![domain model](imgs/domain_model.png)

* A contact can have multiple addresses and has only one name;

* The address is constituted by one street, one zip code and one city.

#### Example of DDD DSL Files

This example of the DDD DSL is for the Contact object, which has a list of addresses as well a contact name. This example defines a new aggretate Contact  which has a list of addresses and has a value object name. A method was also defined to register an address for the contact, called registerAddress.

```java
context axonsample {

	namespace command {

		import axonsample.types.*
		import axonsample.api.*
		import axonsample.constr.*

		/**
		 * A contact in the sample application.
		 */
		aggregate Contact identifier ContactId {

			/** List of addresses. */
			List<Address> addresses

			/** Creates a new classification tree. */
			constructor create {

				/** Identifier to use for the new contact. */
				ContactId id

				/** Name of the contact. */
				ContactName name

				/** A new classification tree was created. */
				event ContactCreatedEvent {

					/** Name of the contact. */
					ContactName name

				}

			}

			/** Registers a new address or updates an existing one. */
			method registerAddress {

				/** Type of the address. */
				AddressType adrType

				/** The address to register. */
				Address address

				/** A new address was added to a contact. */
				event AddressAddedEvent {

					/** Type of the added address. */
					AddressType addressType

					/** New added address. */
					Address address

				}

				/** An existing address of a contact was updated. */
				event AddressChangedEvent {

					/** Type of the changed address. */
					AddressType addressType

					/** Address that was replaced. */
					Address oldAddress

					/** New assigned address. */
					Address newAddress

				}

			}

		}

	}

}
```

#### Result of the transformation of the DDD DSL.

It was generated java code, that essencialy builds the base of the application. Because the methods are created all empty, the developers should implement all the logic required, to fit the business requirements.

```java
package org.fuin.axonsample.command;

import javax.validation.constraints.NotNull;
import org.fuin.axonsample.api.Address;
import org.fuin.axonsample.api.AddressType;
import org.fuin.axonsample.api.ContactId;
import org.fuin.axonsample.api.ContactName;
import org.fuin.ddd4j.ddd.ApplyEvent;

/**
 * A contact in the sample application.
 */
public final class Contact extends AbstractContact {

	/**
	 * Default constructor for loading the aggregate root from history.
	 */
	public Contact() {
		super();
	}

	/**
	 * Creates a new classification tree.
	 *
	 * @param id Identifier to use for the new contact.
	 * @param name Name of the contact.
	 */
	public Contact(@NotNull final ContactId id, @NotNull final ContactName name) {
		super();
		// TODO Implement!
	}

	/**
	 * Registers a new address or updates an existing one.
	 *
	 * @param adrType Type of the address.
	 * @param address The address to register.
	 */
	public final void registerAddress(@NotNull final AddressType adrType, @NotNull final Address address) {
		// TODO Implement!
	}

	/**
	 * Handles: ContactCreatedEvent.
	 *
	 * @param event Event to handle.
	 */
	@Override
	@ApplyEvent
	protected final void handle(@NotNull final ContactCreatedEvent event) {
		// TODO Handle event!
	}

	/**
	 * Handles: AddressAddedEvent.
	 *
	 * @param event Event to handle.
	 */
	@Override
	@ApplyEvent
	protected final void handle(@NotNull final AddressAddedEvent event) {
		// TODO Handle event!
	}

	/**
	 * Handles: AddressChangedEvent.
	 *
	 * @param event Event to handle.
	 */
	@Override
	@ApplyEvent
	protected final void handle(@NotNull final AddressChangedEvent event) {
		// TODO Handle event!
	}

}
```

Demonstration Video (Will be presentend on class)

## Details

To implement a dsl using the ddd approach, we need to understand how this dsl is composed. It has a variety of types of concepts, such as:

### Context / Namespace / Type

The context is referent to the DDD term - Bounded Context.
The namespace refers to the packages that will be created. There's a possibility to add packages as needed just by adding a dot and the name of a package. The type normally is defined outside the dsl.

![png](imgs/example.png)

### Imports

The imports are defined as defined in a class. They are written inside a namespace.

### Constraints

The constraints work as conditions that must be valid. If the condition is not satisfied an error message can be defined. The following schema serves as an example of this type:

![constraint](imgs/constraint.png)

There are multiple types of constraints in this dsl. At least 3 could be retrieved:

- Invariants - constraints that must be kept during the life of the entity. Can only be defined for objects.

![constraint](imgs/invariant.png)

- Pre-conditions - constraints added to a method that require the caller to compel with the rule. Can only be defined for methods. The example below demonstrates that this conditions can be defined as a method level (1) or parameter level (3).

![constraint](imgs/precondition.png)

- Business Rule - these rules are like pre-conditions but the caller is not able to verify them. Can only be defined for methods.

![constraint](imgs/business.png)

### Metadata

Descriptive metadata that describes a variable for purposes like displaying it to the user.

![metadata](imgs/metadata.png)

slabel: Short label used when space in the UI is limited
label: Label for the UI
tooltip: Tooltip for the UI
prompt: Prompt text for input fields
examples: Values that can be used in mockups or tests

### Value Objects

An object that describes some characteristic or attribute but carries no concept of identity. It is created following the structure bellow.

![valueobject](imgs/valueobject.png)

### Aggregate/Entity ID (no documentation on github)

This concept defines the ID of an aggregate or an entity.

![aggregate-id](imgs/aggregate-id.png)

In this example we are defining the id for the Contact aggregate. We have the type of the id set to UUID.

### Enums (no documentation on github)

In the enum concept the enumerations appear inside the instance of the enum.

![enum](imgs/enum.png)

As we can see in the example we have the enum AddressType that can be 3 types of addresses: work, private and vacation.

### Services (no documentation on github)

To create a service we just need to type service and place inside it the methods that the service needs.

![service](imgs/service.png)

In this case the service only have one method to check if the customer exists.

### Aggregates (no documentation on github)

To define an aggregate we just need to define the name of the aggregate and associate the aggregate-id as shown bellow.

![aggregate](imgs/aggregate.png)

### Entities (no documentation on github)

The entity concept describes an entity that must be rooted to an aggregate and have and entity-id associated with.

![entity](imgs/entity.png)

### Events (no documentation on github)

An event is an action that can be triggered when needed.

![event](imgs/event.png)
